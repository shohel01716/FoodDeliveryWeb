<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");
	 
	
	if(isset($_GET['restaurant_id']))
	{
			 
		$rest_qry="SELECT * FROM tbl_restaurants where id='".$_GET['restaurant_id']."'";
    $rest_result=mysqli_query($mysqli,$rest_qry);
    $rest_row=mysqli_fetch_assoc($rest_result);

	}
  else
  {
    header( "Location:manage_restaurants.php");
    exit; 
  }
 
 
  $qry_cat="SELECT COUNT(*) as num FROM tbl_menu_category WHERE restaurant_id='".$_GET['restaurant_id']."'";
  $total_category= mysqli_fetch_array(mysqli_query($mysqli,$qry_cat));
  $total_category = $total_category['num'];

  $qry_menu="SELECT COUNT(*) as num FROM tbl_menu_list WHERE rest_id='".$_GET['restaurant_id']."'";
  $total_menu= mysqli_fetch_array(mysqli_query($mysqli,$qry_menu));
  $total_menu = $total_menu['num'];

  $qry_order="SELECT COUNT(*) as num FROM tbl_order_items WHERE rest_id='".$_GET['restaurant_id']."'";
  $total_order= mysqli_fetch_array(mysqli_query($mysqli,$qry_order));
  $total_order = $total_order['num'];
?>
 
	 <div class="m-grid__item m-grid__item--fluid m-wrapper">
          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title ">
                  Restaurant : <?php echo $rest_row['restaurant_name'];?>
                </h3>
              </div>
              <div>
                 
              </div>
            </div>
          </div>
          <!-- END: Subheader -->
          <div class="m-content">
            <div class="row">
              <div class="col-lg-3">
                <div class="m-portlet m-portlet--full-height ">
                  <div class="m-portlet__body">
                    <div class="m-card-profile">
                      <div class="m-card-profile__title m--hide">
                        Your Dashboard
                      </div>
                      <div class="m-card-profile__pic">
                        <div class="m-card-profile__pic-wrapper">
                          <img src="images/<?php echo $rest_row['restaurant_image'];?>" alt=""/>
                        </div>
                      </div>
                      <div class="m-card-profile__details">
                        <span class="m-card-profile__name">
                          <?php echo $rest_row['restaurant_name'];?>
                        </span>
                         
                          <?php echo $rest_row['restaurant_address'];?>
                         
                      </div>
                    </div>
                    <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                      <li class="m-nav__separator m-nav__separator--fit"></li>
                      <li class="m-nav__section m--hide">
                        <span class="m-nav__section-text">
                          Section
                        </span>
                      </li>                       
                      <li class="m-nav__item">
                        <a href="restaurant_view.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon fa fa-dashboard "></i>
                          <span class="m-nav__link-text">
                            Dashboard
                          </span>
                        </a>
                      </li>
                      <li class="m-nav__item">
                        <a href="manage_menu_category.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon flaticon-share"></i>
                          <span class="m-nav__link-text">
                            Menu Category
                          </span>
                        </a>
                      </li>
                      <li class="m-nav__item">
                        <a href="manage_menu_list.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon flaticon-chat-1"></i>
                          <span class="m-nav__link-text">
                            Menu List
                          </span>
                        </a>
                      </li>
                      <li class="m-nav__item">
                        <a href="manage_rest_order_list.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon fa fa-cart-arrow-down"></i>
                          <span class="m-nav__link-text">
                            Order List
                          </span>
                        </a>
                      </li>
                       
                       
                    </ul>
                    <div class="m-portlet__body-separator"></div>
                    
                  </div>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs ">
                
                  <div class="tab-content">
                     
                     <!--begin:: Widgets/Stats-->
              <div class="m-portlet">
                <div class="m-portlet__body  m-portlet__body--no-padding">
                  <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-6">
                      <!--begin::Total Profit-->
                      <a href="manage_menu_category.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>" style="text-decoration: none;">
                      <div class="m-widget24">
                        <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            Total Category

                          </h4>
                          <br>
                          <span class="m-widget24__desc">
                            All Menu Category
                          </span>
                          <span class="m-widget24__stats m--font-brand">
                            <?php echo $total_category;?>
                          </span>
                          <div class="m--space-40"></div>
                            
                        </div>
                      </div>
                      </a>
                      <!--end::Total Profit-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-6">
                      <!--begin::New Feedbacks-->
                      <a href="manage_menu_list.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>" style="text-decoration: none;">
                      <div class="m-widget24">
                        <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            All Menu
                          </h4>
                          <br>
                          <span class="m-widget24__desc">
                            Menu List
                          </span>
                          <span class="m-widget24__stats m--font-info">
                            <?php echo $total_menu;?>
                          </span>
                          <div class="m--space-40"></div>
                           
                        </div>
                      </div>
                      </a>
                      <!--end::New Feedbacks-->
                    </div>
                    </div>
                    <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-6">
                      <!--begin::New Orders-->
                      <a href="manage_rest_order_list.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>" style="text-decoration: none;">
                      <div class="m-widget24">
                        <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            Order
                          </h4>
                          <br>
                          <span class="m-widget24__desc">
                            Order List
                          </span>
                          <span class="m-widget24__stats m--font-danger">
                            <?php echo $total_order;?>
                          </span>
                          <div class="m--space-40"></div>
                           
                        </div>
                      </div>
                    </a>
                      <!--end::New Orders-->
                    </div>
                    
                  </div>
                </div>
              </div>            
              <!--end:: Widgets/Stats-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->

        
<?php include("includes/footer.php");?>       
