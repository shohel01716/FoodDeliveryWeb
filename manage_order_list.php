<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");
    
 

      $tableName="tbl_order_details";   
      $targetpage = "manage_order_list.php"; 
      $limit = 10; 
      
      $query = "SELECT COUNT(*) as num FROM $tableName  
       ORDER BY tbl_order_details.id DESC";
      $total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
      $total_pages = $total_pages['num'];
      
      $stages = 1;
      $page=0;
      if(isset($_GET['page'])){
      $page = mysqli_real_escape_string($mysqli,$_GET['page']);
      }
      if($page){
        $start = ($page - 1) * $limit; 
      }else{
        $start = 0; 
        }


     $data_qry="SELECT * FROM tbl_order_details
        ORDER BY tbl_order_details.id DESC LIMIT $start, $limit"; 
     $result=mysqli_query($mysqli,$data_qry);
 
	
	if(isset($_GET['order_id']))
	{

		Delete('tbl_order_items','order_id="'.$_GET['order_id'].'"');
    Delete('tbl_order_details','order_unique_id="'.$_GET['order_id'].'"');
 
		$_SESSION['msg']="12";
		header( "Location:manage_order_list.php");
		exit;
		
	}	

//order status
if(isset($_GET['status_pending_id']))
{
   $data = array('status'  =>  $_GET['status_value']);
  
   $edit_status=Update('tbl_order_details', $data, "WHERE order_unique_id = '".$_GET['status_pending_id']."'");
  
   //$_SESSION['msg']="14";
   header( "Location:manage_order_list.php");
   exit;
 }

  function get_user_info($user_id)
   {
    global $mysqli;

    $query1="SELECT * FROM tbl_users
    WHERE tbl_users.id='".$user_id."'";

  $sql1 = mysqli_query($mysqli,$query1)or die(mysqli_error());
  $data1 = mysqli_fetch_assoc($sql1);

  return $data1;
   }

   function get_order_status($order_id)
   {
      global $mysqli;

      $query1="SELECT * FROM tbl_order_details
      WHERE tbl_order_details.order_unique_id='".$order_id."'";

    $sql1 = mysqli_query($mysqli,$query1)or die(mysqli_error());
    $data1 = mysqli_fetch_assoc($sql1);

    return $data1['status'];
   }
	 
?>
                
     <div class="m-grid__item m-grid__item--fluid m-wrapper">
           
          <div class="m-content">
            
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Manage Orders
                       
                    </h3>
                  </div>
                </div>
                <div class="m-portlet__head-tools">
                   
                </div>
              </div>
              <div class="m-portlet__body">
                
                <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?> 
                 
                <!--begin: Datatable -->
                <div class="m_datatable" id="local_data">
                    <table class="table">
              <thead class="thead-default">
                <tr>                  
                   <th>Order ID</th>
                   <th>User Name</th>
                   <th>User Phone</th>
                   <th>Status</th>
                   <th class="cat_action_list">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
            $i=0;
            while($row=mysqli_fetch_array($result))
            {         
        ?>
                <tr scope="row">                 
                   <td><a href="manage_order_list_view.php?order_id=<?php echo $row['order_unique_id'];?>" title="View Order"><?php echo $row['order_unique_id'];?></a></td>
                   <td><?php echo get_user_info($row['user_id'])['name'];?></td>
                   <td><?php echo get_user_info($row['user_id'])['phone'];?></td>
                   <td>
                      <div class="btn-group">
                        <button type="button" class="btn <?php if(get_order_status($row['order_unique_id'])=="Complete"){?>btn-success<?php }else if(get_order_status($row['order_unique_id'])=="Process"){?> btn-warning <?php }else{?>btn-danger<?php }?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo get_order_status($row['order_unique_id']);?></button>
                        <div class="dropdown-menu" x-placement="top-start">
                            <a class="dropdown-item" href="manage_order_list.php?status_pending_id=<?php echo $row['order_unique_id'];?>&status_value=Pending">Pending</a>
                            <a class="dropdown-item" href="manage_order_list.php?status_pending_id=<?php echo $row['order_unique_id'];?>&status_value=Process">Process</a>
                            <a class="dropdown-item" href="manage_order_list.php?status_pending_id=<?php echo $row['order_unique_id'];?>&status_value=Complete">Complete</a>
                            <a class="dropdown-item" href="manage_order_list.php?status_pending_id=<?php echo $row['order_unique_id'];?>&status_value=Cancel">Cancel</a>                            
                             
                        </div>
                      </div>
                    </td>
                   <td>
                          
                   <a href="manage_order_list_view.php?order_id=<?php echo $row['order_unique_id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="View Order">              <i class="la la-eye"></i>            </a>       

                  <a href="?order_id=<?php echo $row['order_unique_id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick="return confirm('Are you sure you want to delete this order?');">              <i class="la la-trash"></i>           </a>
                     
                </tr>
                <?php
            
            $i++;
              }
        ?>    
              
              </tbody>
            </table>
                </div>
          <div class="col-md-12 col-xs-12">
                <div class="pagination_item_block">
                  <nav>
                    <?php include("pagination.php");?>
                  </nav>
                </div>
          </div>


                <!--end: Datatable -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->
<?php include("includes/footer.php");?>       
