<?php include("includes/connection.php");
      include("includes/session_check.php");
      
      //Get file name
      $currentFile = $_SERVER["SCRIPT_NAME"];
      $parts = Explode('/', $currentFile);
      $currentFile = $parts[count($parts) - 1];       
       
      
?>
<!DOCTYPE html>
<html lang="en" >
  <!-- begin::Head -->
  <head>
    <meta charset="utf-8" />
    <title>
     <?php echo APP_NAME;?>
    </title>
    <meta name="description" content="Actions example page">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <!--begin::Page Vendors -->
    <!--end::Page Vendors -->
    <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />

    <script src="assets/ckeditor/ckeditor.js"></script>
  </head>
  <!-- end::Head -->
  <!-- end::Body -->
  <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
      <!-- BEGIN: Header -->
      <header class="m-grid__item    m-header "  data-minimize-mobile="hide" data-minimize-offset="200" data-minimize-mobile-offset="200" data-minimize="minimize" >
        <div class="m-container m-container--fluid m-container--full-height">
          <div class="m-stack m-stack--ver m-stack--desktop">
            <!-- BEGIN: Brand -->
            <div class="m-stack__item m-brand  m-brand--skin-dark ">
              <div class="m-stack m-stack--ver m-stack--general">
                <div class="m-stack__item m-stack__item--middle m-brand__logo">
                  <a href="home.php" class="m-brand__logo-wrapper">
                    <img alt="" src="images/<?php echo APP_LOGO;?>" width="80" height="80"/>
                  </a>
                </div>
                <div class="m-stack__item m-stack__item--middle m-brand__tools">
                  <!-- BEGIN: Left Aside Minimize Toggle -->
                  <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block 
           ">
                    <span></span>
                  </a>
                  <!-- END -->
                  <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                  <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                    <span></span>
                  </a>
                  <!-- END -->
                  <!-- BEGIN: Responsive Header Menu Toggler -->
                  <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                    <span></span>
                  </a>
                  <!-- END -->
                  <!-- BEGIN: Topbar Toggler -->
                  <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                    <i class="flaticon-more"></i>
                  </a>
                  <!-- BEGIN: Topbar Toggler -->
                </div>
              </div>
            </div>
            <!-- END: Brand -->
            <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
              
              <!-- BEGIN: Topbar -->
              <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                <div class="m-stack__item m-topbar__nav-wrapper">
                  <ul class="m-topbar__nav m-nav m-nav--inline">
                     
                    
                    <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
                      <a href="#" class="m-nav__link m-dropdown__toggle">
                        <span class="m-topbar__userpic">
                          
                          <?php if(PROFILE_IMG!=''){?>
                                <img src="images/<?php echo PROFILE_IMG;?>" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                            <?php } else{?>
                                <img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                            <?php }?>
                        </span>
                        <span class="m-topbar__username m--hide">
                          Nick
                        </span>
                      </a>
                      <div class="m-dropdown__wrapper">
                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                        <div class="m-dropdown__inner">
                          <div class="m-dropdown__header m--align-center" style="background: url(assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
                            <div class="m-card-user m-card-user--skin-dark">
                              <div class="m-card-user__pic">
                              <?php if(PROFILE_IMG!=''){?>
                                <img src="images/<?php echo PROFILE_IMG;?>" class="m--img-rounded m--marginless" alt=""/>
                            <?php } else{?>
                                <img src="assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt=""/>
                            <?php }?>
                                
                                
                              </div>
                              <div class="m-card-user__details">
                                <span class="m-card-user__name m--font-weight-500">
                                  <?php echo PROFILE_NAME;?>
                                </span>
                                <a href="mailto:<?php echo PROFILE_EMAIL;?>" class="m-card-user__email m--font-weight-300 m-link">
                                  <?php echo PROFILE_EMAIL;?>
                                </a>
                              </div>
                            </div>
                          </div>
                          <div class="m-dropdown__body">
                            <div class="m-dropdown__content">
                              <ul class="m-nav m-nav--skin-light">
                                <li class="m-nav__section m--hide">
                                  <span class="m-nav__section-text">
                                    Section
                                  </span>
                                </li>
                                <li class="m-nav__item">
                                  <a href="profile.php" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-profile-1"></i>
                                    <span class="m-nav__link-title">
                                      <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                          My Profile
                                        </span>
                                         
                                      </span>
                                    </span>
                                  </a>
                                </li>
                                <li class="m-nav__item">
                                  <a href="settings.php" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-cogwheel-2"></i>
                                    <span class="m-nav__link-title">
                                      <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                          Settings
                                        </span>
                                         
                                      </span>
                                    </span>
                                  </a>
                                </li>
                                 
                                <li class="m-nav__separator m-nav__separator--fit"></li>
                                <li class="m-nav__item">
                                  <a href="logout.php" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                    Logout
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                     
                  </ul>
                </div>
              </div>
              <!-- END: Topbar -->
            </div>
          </div>
        </div>
      </header>
      <!-- END: Header -->
      <!-- begin::Body -->
      <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
          <!-- BEGIN: Aside Menu -->
          <div 
    id="m_ver_menu" 
    class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
    data-menu-vertical="true"
     data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
    >
            <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
              <li class="m-menu__item <?php if($currentFile=="home.php"){?>m-menu__item--active<?php }?>" aria-haspopup="true" >
                <a  href="home.php" class="m-menu__link ">
                  <i class="m-menu__link-icon flaticon-line-graph"></i>
                  <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                      <span class="m-menu__link-text">
                        Dashboard
                      </span>
                      
                    </span>
                  </span>
                </a>
              </li>
 
              
              <li class="m-menu__item <?php if($currentFile=="manage_category.php" or $currentFile=="add_category.php"){?>m-menu__item--active<?php }?> aria-haspopup="true" >
                <a  href="manage_category.php" class="m-menu__link ">
                  <i class="m-menu__link-icon flaticon-list"></i>
                  <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                      <span class="m-menu__link-text">
                        Category
                      </span>
                      
                    </span>
                  </span>
                </a>
              </li>
              <li class="m-menu__item <?php if($currentFile=="manage_restaurants.php" or $currentFile=="add_restaurant.php"){?>m-menu__item--active<?php }?> aria-haspopup="true" >
                <a  href="manage_restaurants.php" class="m-menu__link ">
                  <i class="m-menu__link-icon fa fa-coffee"></i>
                  <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                      <span class="m-menu__link-text">
                        Restaurants
                      </span>
                      
                    </span>
                  </span>
                </a>
              </li>               
              
              
              
              <li class="m-menu__item <?php if($currentFile=="manage_users.php" or $currentFile=="add_user.php"){?>m-menu__item--active<?php }?> aria-haspopup="true" >
                <a  href="manage_users.php" class="m-menu__link ">
                  <i class="m-menu__link-icon fa fa-users"></i>
                  <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                      <span class="m-menu__link-text">
                        Users
                      </span>
                      
                    </span>
                  </span>
                </a>
              </li>

              <li class="m-menu__item <?php if($currentFile=="manage_order_list.php" or $currentFile=="manage_order_list_view.php"){?>m-menu__item--active<?php }?> aria-haspopup="true" >
                <a  href="manage_order_list.php" class="m-menu__link ">
                  <i class="m-menu__link-icon fa fa-cart-arrow-down"></i>
                  <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                      <span class="m-menu__link-text">
                        Order List
                      </span>
                      
                    </span>
                  </span>
                </a>
              </li>
			   <li class="m-menu__item <?php if($currentFile=="send_notification.php"){?>m-menu__item--active<?php }?> aria-haspopup="true" >
                <a  href="send_notification.php" class="m-menu__link">
                 <i class=" m-menu__link-icon fa fa-send"  aria-hidden="true"></i>
                  <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                      <span class="m-menu__link-text">
                        Notification
                      </span>
                      
                    </span>
                  </span>
                </a>
              </li>
                 
              <li class="m-menu__item <?php if($currentFile=="settings.php"){?>m-menu__item--active<?php }?> aria-haspopup="true" >
                <a  href="settings.php" class="m-menu__link ">
                  <i class="m-menu__link-icon flaticon-cogwheel-2"></i>
                  <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                      <span class="m-menu__link-text">
                        Settings
                      </span>
                      
                    </span>
                  </span>
                </a>
              </li>
              <li class="m-menu__item <?php if($currentFile=="api_urls.php"){?>m-menu__item--active<?php }?> aria-haspopup="true" >
                <a  href="api_urls.php" class="m-menu__link ">
                  <i class="m-menu__link-icon fa fa-exchange"></i>
                  <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                      <span class="m-menu__link-text">
                        API URL
                      </span>
                      
                    </span>
                  </span>
                </a>
              </li> 
       
            </ul>
          </div>
          <!-- END: Aside Menu -->
        </div>