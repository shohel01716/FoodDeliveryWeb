<!-- begin::Footer -->
			<footer class="m-grid__item		m-footer ">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
 								Copyright &copy; <?php echo date('Y');?> <a href="http://www.pingfood.com" target="_blank">pingfood.com</a>. All Rights Reserved.
							</span>
						</div>
						 
					</div>
				</div>
			</footer>
			<!-- end::Footer -->
		</div>
		<!-- end:: Page -->
		 
		<!-- begin::Scroll Top -->
		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end::Scroll Top -->
		 
		<!--begin::Base Scripts -->
		<script src="assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<!--end::Base Scripts -->
		<!--begin::Page Resources -->
		<script src="assets/demo/default/custom/header/actions.js" type="text/javascript"></script>
 		<!--end::Page Resources -->

		<!--begin::Page Snippets -->
		<script src="assets/app/js/dashboard.js" type="text/javascript"></script>
		
		<!--end::Page Snippets -->
		<script type="text/javascript">
            
			$('#m_select1').select2({
            placeholder: "Select a Category"
        });
 

		</script>
	</body>
	<!-- end::Body -->
</html>
