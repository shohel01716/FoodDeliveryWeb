<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");
	 
	 $qry="SELECT * FROM tbl_settings where id='1'";
  $result=mysqli_query($mysqli,$qry);
  $settings_row=mysqli_fetch_assoc($result);

 

  if(isset($_POST['submit']))
  {

    

    $img_res=mysqli_query($mysqli,"SELECT * FROM tbl_settings WHERE id='1'");
    $img_row=mysqli_fetch_assoc($img_res);
    

    if($_FILES['app_logo']['name']!="")
    {        

            unlink('images/'.$img_row['app_logo']);   

            $app_logo=$_FILES['app_logo']['name'];
            $pic1=$_FILES['app_logo']['tmp_name'];

            $tpath1='images/'.$app_logo;      
            copy($pic1,$tpath1);


              $data = array(      
                
              'app_name'  =>  $_POST['app_name'],
              'app_logo'  =>  $app_logo,  
              'app_description'  => addslashes($_POST['app_description']),
              'app_version'  =>  $_POST['app_version'],
              'app_author'  =>  $_POST['app_author'],
              'app_contact'  =>  $_POST['app_contact'],
              'app_email'  =>  $_POST['app_email'],   
              'app_website'  =>  $_POST['app_website'],
              'app_developed_by'  =>  $_POST['app_developed_by']                     

              );

    }
    else
    {
  
                $data = array(

                'app_name'  =>  $_POST['app_name'],
                'app_description'  => addslashes($_POST['app_description']),
                'app_version'  =>  $_POST['app_version'],
                'app_author'  =>  $_POST['app_author'],
                'app_contact'  =>  $_POST['app_contact'],
                'app_email'  =>  $_POST['app_email'],   
                'app_website'  =>  $_POST['app_website'],
                'app_developed_by'  =>  $_POST['app_developed_by']               

                  );

    } 

    $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");
 

      if ($settings_edit > 0)
      {

        $_SESSION['msg']="11";
        header( "Location:settings.php");
        exit;

      }   
 
  }

if(isset($_POST['notification_submit']))
  {

        $data = array(
                'onesignal_app_id' => $_POST['onesignal_app_id'],
                'onesignal_rest_key' => $_POST['onesignal_rest_key'],
                  );

    
      $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");
  
 
        $_SESSION['msg']="11";
        header( "Location:settings.php");
        exit;
 
  }
  if(isset($_POST['api_submit']))
  {

        $data = array(

                'api_latest_limit'  =>  $_POST['api_latest_limit'],
                'api_cat_order_by'  =>  $_POST['api_cat_order_by'],
                'api_cat_post_order_by'  =>  $_POST['api_cat_post_order_by'] 
                  );

    
      $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");
 

      if ($settings_edit > 0)
      {

        $_SESSION['msg']="11";
        header( "Location:settings.php");
        exit;

      }   
 
  }
  if(isset($_POST['admob_submit']))
  {

        $data = array(
                'publisher_id'  =>  $_POST['publisher_id'],
                'interstital_ad'  =>  $_POST['interstital_ad'],
                'interstital_ad_id'  =>  $_POST['interstital_ad_id'],
                'interstital_ad_click'  =>  $_POST['interstital_ad_click'],
                'banner_ad'  =>  $_POST['banner_ad'],
                'banner_ad_id'  =>  $_POST['banner_ad_id']
                  );

    
      $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");
   
        $_SESSION['msg']="11";
        header( "Location:settings.php");
        exit;
  
  }

  if(isset($_POST['app_pri_poly']))
  {

        $data = array(
                'app_privacy_policy'  =>  addslashes($_POST['app_privacy_policy'])
                  );

    
      $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");
 

      if ($settings_edit > 0)
      {

        $_SESSION['msg']="11";
        header( "Location:settings.php");
        exit;

      }   
 
  }

  if(isset($_POST['app_terms_con']))
  {

        $data = array(
                'app_terms_conditions'  =>  addslashes($_POST['app_terms_conditions']) 
                  );

    
      $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");
  
        $_SESSION['msg']="11";
        header( "Location:settings.php");
        exit;
 
 
  }


  if(isset($_POST['app_email_config']))
  {

        $data = array(
                'app_from_email'  =>  $_POST['app_from_email'],
                'app_admin_email'  =>  $_POST['app_admin_email'] 
                  );

    
      $settings_edit=Update('tbl_settings', $data, "WHERE id = '1'");
  
        $_SESSION['msg']="11";
        header( "Location:settings.php");
        exit;
 
 
  }
  


?>
 
	 <div class="m-grid__item m-grid__item--fluid m-wrapper">
          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title ">
                  Settings
                </h3>
                 
              </div>
              <div>
                 
              </div>
            </div>
          </div>
          <!-- END: Subheader -->
          <div class="m-content">
            <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?> 
            <div class="row">               
              <div class="col-lg-12">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs ">
                  <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                      <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                        <li class="nav-item m-tabs__item">
                          <a class="nav-link m-tabs__link active" data-toggle="tab" href="#app_settings_tab_1" role="tab">
                            <i class="flaticon-share m--hide"></i>
                            App Settings
                          </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                          <a class="nav-link m-tabs__link" data-toggle="tab" href="#app_email_config" role="tab">
                             Email 
                          </a>
                        </li>
						<li class="nav-item m-tabs__item">
                          <a class="nav-link m-tabs__link" data-toggle="tab" href="#ad_settings" role="tab">
                             Ad
                          </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                          <a class="nav-link m-tabs__link" data-toggle="tab" href="#app_settings_tab_2" role="tab">
                             API
                          </a>
                        </li>
						<li class="nav-item m-tabs__item">
                          <a class="nav-link m-tabs__link" data-toggle="tab" href="#notification_settings" role="tab">
                            Notification
                          </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                          <a class="nav-link m-tabs__link" data-toggle="tab" href="#app_settings_tab_3" role="tab">
                           Privacy Policy
                          </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                          <a class="nav-link m-tabs__link" data-toggle="tab" href="#app_settings_tab_4" role="tab">
                             Terms & Condition
                          </a>
                        </li>
                        
                      </ul>
                    </div>
                     
                  </div>
                  <div class="tab-content">
                    <div class="tab-pane active" id="app_settings_tab_1">
                      <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                        <div class="m-portlet__body">
                           
                           
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                              App Name
                            </label>
                            <div class="col-7">
                              <input class="form-control m-input" type="text" name="app_name" value="<?php echo $settings_row['app_name'];?>">
                            </div>
                          </div>
                          
                           <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                              Profile Image
                            </label>
                            <div class="col-7">
                               <input type="file" id="" name="app_logo" class="form-control m-input">
                                  <br/>
                                  <?php if($settings_row['app_logo']!="") {?>
                                    <img type="image" src="images/<?php echo $settings_row['app_logo'];?>" alt="image" />
                                  <?php } else {?>
                                    <img type="image" src="assets/images/add-image.png" alt="image" />
                                  <?php }?>

                             </div>
                          </div>
                          <div class="form-group m-form__group row">
                              <label class="col-2 col-form-label">App Description :-</label>
                              <div class="col-md-6">
                                <textarea name="app_description" id="app_description" class="form-control m-input"><?php echo stripslashes($settings_row['app_description']);?></textarea>

                      <script>CKEDITOR.replace( 'app_description' );</script>
                              </div>
                          </div>
                          <div class="form-group">&nbsp;</div>                 
                            <div class="form-group m-form__group row">
                              <label class="col-2 col-form-label">App Version :-</label>
                              <div class="col-md-6">
                                <input type="text" name="app_version" id="app_version" value="<?php echo $settings_row['app_version'];?>" class="form-control">
                              </div>
                            </div>
                            <div class="form-group m-form__group row">
                              <label class="col-2 col-form-label">Author :-</label>
                              <div class="col-md-6">
                                <input type="text" name="app_author" id="app_author" value="<?php echo $settings_row['app_author'];?>" class="form-control">
                              </div>
                            </div>
                            <div class="form-group m-form__group row">
                              <label class="col-2 col-form-label">Contact :-</label>
                              <div class="col-md-6">
                                <input type="text" name="app_contact" id="app_contact" value="<?php echo $settings_row['app_contact'];?>" class="form-control">
                              </div>
                            </div>     
                            <div class="form-group m-form__group row">
                              <label class="col-2 col-form-label">Email :-</label>
                              <div class="col-md-6">
                                <input type="text" name="app_email" id="app_email" value="<?php echo $settings_row['app_email'];?>" class="form-control">
                              </div>
                            </div>                 
                             <div class="form-group m-form__group row">
                              <label class="col-2 col-form-label">Website :-</label>
                              <div class="col-md-6">
                                <input type="text" name="app_website" id="app_website" value="<?php echo $settings_row['app_website'];?>" class="form-control">
                              </div>
                            </div>
                            <div class="form-group m-form__group row">
                              <label class="col-2 col-form-label">Developed By :-</label>
                              <div class="col-md-6">
                                <input type="text" name="app_developed_by" id="app_developed_by" value="<?php echo $settings_row['app_developed_by'];?>" class="form-control">
                              </div>
                            </div> 
                            
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                          <div class="m-form__actions">
                            <div class="row">
                              <div class="col-2"></div>
                              <div class="col-7">
                                <button type="submit" name="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                  Save changes
                                </button>
                               
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane" id="app_email_config">
                      <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                        <div class="m-portlet__body">
                           
                      
                          <div class="form-group m-form__group row">
                              <label class="col-2 col-form-label">Server Email :-<br><small>Domain email E.g.: info@example.com</small></label>
                              <div class="col-md-6">
                                <input type="email" name="app_from_email" id="app_from_email" value="<?php echo $settings_row['app_from_email'];?>" class="form-control" required>
                              </div>
                            </div>

                          <div class="form-group m-form__group row">
                              <label class="col-2 col-form-label">Order Notification Email :-</label>
                              <div class="col-md-6">
                                <input type="email" name="app_admin_email" id="app_admin_email" value="<?php echo $settings_row['app_admin_email'];?>" class="form-control" required>
                              </div>
                            </div>       
                           
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                          <div class="m-form__actions">
                            <div class="row">
                              <div class="col-2"></div>
                              <div class="col-7">
                                <button type="submit" name="app_email_config" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                  Save changes
                                </button>
                               
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
					<div class="tab-pane" id="ad_settings">
                      <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                        <div class="m-portlet__body">
                           
                          <div class="form-group m-form__group row">
                  <label class="col-md-2 control-label">Publisher ID :-</label>
                  <div class="col-md-7">
                    <input type="text" name="publisher_id" id="publisher_id" value="<?php echo $settings_row['publisher_id'];?>" class="form-control m-input">
                  </div>
                  </div>
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
							Banner Ad:-
                            </label>
                            <div class="col-7">
											<select name="banner_ad" id="banner_ad" class="form-control m-input">
                                <option value="true" <?php if($settings_row['banner_ad']=='true'){?>selected<?php }?>>True</option>
                                <option value="false" <?php if($settings_row['banner_ad']=='false'){?>selected<?php }?>>False</option>
                    
                            </select>                            
							</div>
                          </div>
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
							Banner Ad ID :-
                            </label>
                            <div class="col-7">
                          <input type="text" name="banner_ad_id" id="banner_ad_id" value="<?php echo $settings_row['banner_ad_id'];?>" class="form-control m-input">

                            </div>
                          </div>
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
								Interstital Ads:-                            
								</label>
                            <div class="col-7">
                              <select name="interstital_ad" id="interstital_ad" class="form-control m-input">
                                <option value="true" <?php if($settings_row['interstital_ad']=='true'){?>selected<?php }?>>True</option>
                                <option value="false" <?php if($settings_row['interstital_ad']=='false'){?>selected<?php }?>>False</option>
                    
                            </select> 
                            </div>
                          </div>
						  <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
							Interstital Ad ID :-
                            </label>
                            <div class="col-7">
                            <input type="text" name="interstital_ad_id" id="interstital_ad_id" value="<?php echo $settings_row['interstital_ad_id'];?>" class="form-control m-input">

                            </div>
                          </div><div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
							Interstital Ad Clicks :-
                            </label>
                            <div class="col-7">
                            <input type="text" name="interstital_ad_click" id="interstital_ad_click" value="<?php echo $settings_row['interstital_ad_click'];?>" class="form-control m-input">

                            </div>
                          </div>

                        </div> 
                        <div class="m-portlet__foot m-portlet__foot--fit">
                          <div class="m-form__actions">
                            <div class="row">
                              <div class="col-2"></div>
                              <div class="col-7">
                                <button type="submit" name="admob_submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                  Save changes
                                </button>
                               
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
					
							<div class="tab-pane" id="notification_settings">
                      <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                        <div class="m-portlet__body">
                           
                          <div class="form-group m-form__group row">
                  <label class="col-md-2 control-label">OneSignal App ID :-</label>
                  <div class="col-md-7">
                      <input type="text" name="onesignal_app_id" id="onesignal_app_id" value="<?php echo $settings_row['onesignal_app_id'];?>" class="form-control m-input">
                  </div>
                  </div>
                          
						  <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
							OneSignal Rest Key :-
                            </label>
                            <div class="col-7">
                      <input type="text" name="onesignal_rest_key" id="onesignal_rest_key" value="<?php echo $settings_row['onesignal_rest_key'];?>" class="form-control m-input">

                            </div>
                          </div>

                        </div> 
                        <div class="m-portlet__foot m-portlet__foot--fit">
                          <div class="m-form__actions">
                            <div class="row">
                              <div class="col-2"></div>
                              <div class="col-7">
                                <button type="submit" name="notification_submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                  Save changes
                                </button>
                               
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane" id="app_settings_tab_2">
                      <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                        <div class="m-portlet__body">
                           
                           
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                              Latest Limit
                            </label>
                            <div class="col-7">
                              <input class="form-control m-input" type="text" name="api_latest_limit" value="<?php echo $settings_row['api_latest_limit'];?>">
                            </div>
                          </div>
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                              Program List Order By
                            </label>
                            <div class="col-7">
                              <select class="form-control m-input" name="api_cat_order_by" id="api_cat_order_by">
                  <option value="pid" <?php if($settings_row['api_cat_order_by']=='pid'){?>selected<?php }?>>ID</option>
                                <option value="program_name" <?php if($settings_row['api_cat_order_by']=='program_name'){?>selected<?php }?>>Name</option>
               </select>
                            </div>
                          </div>
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                              Program Video Order
                            </label>
                            <div class="col-7">
                              <select class="form-control m-input" name="api_cat_post_order_by" id="api_cat_post_order_by">
                   <option value="ASC" <?php if($settings_row['api_cat_post_order_by']=='ASC'){?>selected<?php }?>>ASC</option>
                          <option value="DESC" <?php if($settings_row['api_cat_post_order_by']=='DESC'){?>selected<?php }?>>DESC</option>
               </select>
                            </div>
                          </div>

                        </div> 
                        <div class="m-portlet__foot m-portlet__foot--fit">
                          <div class="m-form__actions">
                            <div class="row">
                              <div class="col-2"></div>
                              <div class="col-7">
                                <button type="submit" name="api_submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                  Save changes
                                </button>
                               
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>

                    <div class="tab-pane" id="app_settings_tab_3">
                      <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                        <div class="m-portlet__body">
                           
                      
                          <div class="form-group m-form__group row">
                              <label class="col-2 col-form-label">App Privacy Policy :-</label>
                              <div class="col-md-6">
                                <textarea name="app_privacy_policy" id="app_privacy_policy" class="form-control m-input"><?php echo stripslashes($settings_row['app_privacy_policy']);?></textarea>

                                <script>CKEDITOR.replace( 'app_privacy_policy' );</script>
                              </div>
                          </div>
                           
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                          <div class="m-form__actions">
                            <div class="row">
                              <div class="col-2"></div>
                              <div class="col-7">
                                <button type="submit" name="app_pri_poly" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                  Save changes
                                </button>
                               
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>

                    <div class="tab-pane" id="app_settings_tab_4">
                      <form class="m-form m-form--fit m-form--label-align-right" action="settings.php" method="post" enctype="multipart/form-data">
                        <div class="m-portlet__body">
                           
                      
                          <div class="form-group m-form__group row">
                              <label class="col-2 col-form-label">App Terms Conditions :-</label>
                              <div class="col-md-6">
                                <textarea name="app_terms_conditions" id="app_terms_conditions" class="form-control m-input"><?php echo stripslashes($settings_row['app_terms_conditions']);?></textarea>

                                <script>CKEDITOR.replace( 'app_terms_conditions' );</script>
                              </div>
                          </div>
                           
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                          <div class="m-form__actions">
                            <div class="row">
                              <div class="col-2"></div>
                              <div class="col-7">
                                <button type="submit" name="app_terms_con" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                  Save changes
                                </button>
                               
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->

        
<?php include("includes/footer.php");?>       
