<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");
    

    if(isset($_GET['restaurant_id']))
    {
         
      $rest_qry="SELECT * FROM tbl_restaurants where id='".$_GET['restaurant_id']."'";
      $rest_result=mysqli_query($mysqli,$rest_qry);
      $rest_row=mysqli_fetch_assoc($rest_result);

    }
    else
    {
      header("Location:manage_restaurants.php");
      exit; 
    }
      

       


     $data_qry="SELECT * FROM tbl_order_items
       WHERE tbl_order_items.order_id='".$_GET['order_id']."' AND tbl_order_items.rest_id=".$_GET['restaurant_id']." 
       ORDER BY tbl_order_items.id DESC"; 
     $result=mysqli_query($mysqli,$data_qry);
 
	 
  function get_user_info($user_id)
   {
    global $mysqli;

    $query1="SELECT * FROM tbl_users
    WHERE tbl_users.id='".$user_id."'";

  $sql1 = mysqli_query($mysqli,$query1)or die(mysqli_error());
  $data1 = mysqli_fetch_assoc($sql1);

  return $data1;
   }
	 
?>
                
     <div class="m-grid__item m-grid__item--fluid m-wrapper">
          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title ">
                  Restaurant : <?php echo $rest_row['restaurant_name'];?>
                </h3>
              </div>
              <div>
                 
              </div>
            </div>
          </div>
          <!-- END: Subheader -->
          <div class="m-content">
            <div class="row">
              <div class="col-lg-3">
                <div class="m-portlet m-portlet--full-height ">
                  <div class="m-portlet__body">
                    <div class="m-card-profile">
                      <div class="m-card-profile__title m--hide">
                        Your Dashboard
                      </div>
                      <div class="m-card-profile__pic">
                        <div class="m-card-profile__pic-wrapper">
                          <img src="images/<?php echo $rest_row['restaurant_image'];?>" alt=""/>
                        </div>
                      </div>
                      <div class="m-card-profile__details">
                        <span class="m-card-profile__name">
                          <?php echo $rest_row['restaurant_name'];?>
                        </span>
                         
                          <?php echo $rest_row['restaurant_address'];?>
                         
                      </div>
                    </div>
                    <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                      <li class="m-nav__separator m-nav__separator--fit"></li>
                      <li class="m-nav__section m--hide">
                        <span class="m-nav__section-text">
                          Section
                        </span>
                      </li>
                       <li class="m-nav__item">
                        <a href="restaurant_view.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon fa fa-dashboard "></i>
                          <span class="m-nav__link-text">
                            Dashboard
                          </span>
                        </a>
                      </li>
                      <li class="m-nav__item">
                        <a href="manage_menu_category.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon flaticon-share"></i>
                          <span class="m-nav__link-text">
                            Menu Category
                          </span>
                        </a>
                      </li>
                      <li class="m-nav__item">
                        <a href="manage_menu_list.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon flaticon-chat-1"></i>
                          <span class="m-nav__link-text">
                            Menu List
                          </span>
                        </a>
                      </li>
                      <li class="m-nav__item">
                        <a href="manage_rest_order_list.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon fa fa-cart-arrow-down"></i>
                          <span class="m-nav__link-text">
                            Order List
                          </span>
                        </a>
                      </li>
                       
                    </ul>
                    <div class="m-portlet__body-separator"></div>
                    
                  </div>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="m-content">
            
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Order : <?php echo $_GET['order_id'];?>
                       
                    </h3>
                  </div>
                </div>
                <div class="m-portlet__head-tools">
                   
                </div>
              </div>
              <div class="m-portlet__body">

                 <!--begin: Search Form -->

                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                  <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                      <div class="form-group m-form__group row align-items-center">
                        <form  method="post" action="" class="m-form">

                            <div class="col-md-12">
                              <div class="m-input-icon m-input-icon--left">
                                 <b>Name</b>: <?php echo get_user_info($_GET['user_id'])['name'];?><br>
                                 <b>Email:</b>: <?php echo get_user_info($_GET['user_id'])['email'];?><br>
                                 <b>Phone:</b>: <?php echo get_user_info($_GET['user_id'])['phone'];?><br>
                                 <b>Address:</b>: <?php echo get_user_info($_GET['user_id'])['address'];?>
                              </div>
                            </div>
                        </form>    
                      </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                      <a href="manage_rest_order_list.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                        <span>
                          <i class="fa fa-arrow-left"></i>
                          <span>
                            Back
                          </span>
                        </span>
                      </a>
                      <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                  </div>
                </div>
                <!--end: Search Form -->

                 
                <!--begin: Datatable -->
                <div class="m_datatable" id="local_data">
                    <table class="table">
              <thead class="thead-default">
                <tr>                  
                   <th>Menu Name</th>
                   <th>QTY</th>
                   <th>Price</th>
                   <th>Sub Total</th>
                    
                 </tr>
              </thead>
              <tbody>
                <?php 
            $i=0;
            $total_price=0;
            while($row=mysqli_fetch_array($result))
            {         
        ?>
                <tr scope="row">                 
                    <td><?php echo $row['menu_name'];?></td>
                    <td><?php echo $row['menu_qty'];?></td>
                    <td><?php echo $row['menu_price'];?></td>
                    <td><?php echo $row['menu_total_price'];?></td>
                        
                     
                </tr>
                <?php
                $total_price=$total_price+$row['menu_total_price'];
            $i++;
              }
        ?>      
                <tr>                 
                   <td>&nbsp;</td>
                   <td>&nbsp;</td>
                   <td>&nbsp;</td>
                   <td><b>Total</b></td>
                   <td>
                       <?php echo number_format(round((float)$total_price,2),2);?>   
                   </td>
                     
                </tr>

              </tbody>
            </table>

                </div>
                <div class="col-md-12 col-xs-12">
                <div class="pagination_item_block">
                  <nav>
                    <?php include("pagination_rest.php");?>
                  </nav>
                </div>
          </div>
                <!--end: Datatable -->
              </div>
            </div>
          </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->
        
<?php include("includes/footer.php");?>       
