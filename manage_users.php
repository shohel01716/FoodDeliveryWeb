<?php include('includes/header.php'); 

    include('includes/function.php');
	include('language/language.php');  


	if(isset($_POST['user_search']))
	 {
		 
		
		$user_qry="SELECT * FROM tbl_users WHERE tbl_users.name like '%".addslashes($_POST['search_value'])."%' or tbl_users.email like '%".addslashes($_POST['search_value'])."%' ORDER BY tbl_users.id DESC";  
							 
		$users_result=mysqli_query($mysqli,$user_qry);
		
		 
	 }
	 else
	 {
	 
							$tableName="tbl_users";		
							$targetpage = "manage_users.php"; 	
							$limit = 15; 
							
							$query = "SELECT COUNT(*) as num FROM $tableName";
							$total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
							$total_pages = $total_pages['num'];
							
							$stages = 3;
							$page=0;
							if(isset($_GET['page'])){
							$page = mysqli_real_escape_string($mysqli,$_GET['page']);
							}
							if($page){
								$start = ($page - 1) * $limit; 
							}else{
								$start = 0;	
								}	
							
							
						 $users_qry="SELECT * FROM tbl_users
						 ORDER BY tbl_users.id DESC LIMIT $start, $limit";  
							 
							$users_result=mysqli_query($mysqli,$users_qry);
							
	 }
	if(isset($_GET['user_id']))
	{
		  
		 
		Delete('tbl_users','id='.$_GET['user_id'].'');
		
		$_SESSION['msg']="12";
		header( "Location:manage_users.php");
		exit;
	}
	
	//Active and Deactive status
	if(isset($_GET['status_deactive_id']))
	{
		$data = array('status'  =>  '0');
		
		$edit_status=Update('tbl_users', $data, "WHERE id = '".$_GET['status_deactive_id']."'");
		
		 $_SESSION['msg']="14";
		 header( "Location:manage_users.php");
		 exit;
	}
	if(isset($_GET['status_active_id']))
	{
		$data = array('status'  =>  '1');
		
		$edit_status=Update('tbl_users', $data, "WHERE id = '".$_GET['status_active_id']."'");
		
		$_SESSION['msg']="13";
		 header( "Location:manage_users.php");
		 exit;
	}
	
	
?>


 <div class="m-grid__item m-grid__item--fluid m-wrapper">
           
          <div class="m-content">
            
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Manage Users
                       
                    </h3>
                  </div>
                </div>
                <div class="m-portlet__head-tools">
                   
                </div>
              </div>
              <div class="m-portlet__body">
                
                <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?> 
                <!--begin: Search Form -->

                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                  <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                      <div class="form-group m-form__group row align-items-center">
                        <form  method="post" action="" class="m-form">

                            <div class="col-md-12">
                              <div class="m-input-icon m-input-icon--left">
                                <div class="input-group">
                                  <input type="text" name="search_value" class="form-control form-control-warning" placeholder="Search for..." required>
                                  <span class="input-group-btn">
                                    <button class="btn btn-brand" type="submit" name="user_search">
                                      Go!
                                    </button>
                                  </span>
                                </div>
                              </div>
                            </div>
                        </form>    
                      </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                      <a href="add_user.php?add=yes" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                        <span>
                          <i class="la la-plus"></i>
                          <span>
                            Add User
                          </span>
                        </span>
                      </a>
                      <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                  </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="m_datatable" id="local_data">
                    <table class="table">
              <thead class="thead-default">
                <tr>                  
                  <th>Name</th>						 
        				  <th>Email</th>
        				  <th>Phone</th>
         				  <th>Status</th>	 
                  <th class="cat_action_list">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
            $i=0;
            while($users_row=mysqli_fetch_array($users_result))
            {         
        ?>
                <tr scope="row">                 
                  <td><?php echo $users_row['name'];?></td>
		           <td><?php echo $users_row['email'];?></td>   
		           <td><?php echo $users_row['phone'];?></td>             
		           <td>
		          		<?php if($users_row['status']!="0"){?>
		              <a href="manage_users.php?status_deactive_id=<?php echo $users_row['id'];?>" title="Change Status"><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span style="font-size: 12px;
    font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;">Enable</span></span></a>

		              <?php }else{?>
		              <a href="manage_users.php?status_active_id=<?php echo $users_row['id'];?>" title="Change Status"><span class="badge badge-danger badge-icon"><i class="fa fa-close" aria-hidden="true"></i><span style="font-size: 12px;
    font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;">Disable </span></span></a>
		              <?php }?>
                </td>
                <td>
                    <a href="add_user.php?user_id=<?php echo $users_row['id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">              <i class="la la-edit"></i>            </a>

                  <a href="?user_id=<?php echo $users_row['id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick="return confirm('Are you sure you want to delete this user?');">              <i class="la la-trash"></i>           </a>
                     
                </tr>
                <?php
            
            $i++;
              }
        ?> 
              </tbody>
            </table>

                </div>
          <div class="col-md-12 col-xs-12">
                <div class="pagination_item_block">
                  <nav>
                    <?php if(!isset($_POST["user_search"])){ include("pagination.php");}?>
                  </nav>
                </div>
          </div>


                <!--end: Datatable -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->
        
<?php include("includes/footer.php");?>    