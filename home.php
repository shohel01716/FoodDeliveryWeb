<?php include("includes/header.php");


$qry_cat="SELECT COUNT(*) as num FROM tbl_category";
$total_category= mysqli_fetch_array(mysqli_query($mysqli,$qry_cat));
$total_category = $total_category['num'];

$qry_restaurant="SELECT COUNT(*) as num FROM tbl_restaurants";
$total_restaurant = mysqli_fetch_array(mysqli_query($mysqli,$qry_restaurant));
$total_restaurant = $total_restaurant['num'];
 

$qry_users="SELECT COUNT(*) as num FROM tbl_users";
$total_users = mysqli_fetch_array(mysqli_query($mysqli,$qry_users));
$total_users = $total_users['num'];

 
$qry_order="SELECT COUNT(*) as num FROM tbl_order_details";
$total_order = mysqli_fetch_array(mysqli_query($mysqli,$qry_order));
$total_order = $total_order['num'];
 
?>       


        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title ">
                  Dashboard
                </h3>
              </div>
               
            </div>
          </div>
          <!-- END: Subheader -->
          <div class="m-content">
             
            <!--Begin::Main Portlet-->
              <!--begin:: Widgets/Stats-->
              <div class="m-portlet">
                <div class="m-portlet__body  m-portlet__body--no-padding">
                  <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-3">
                      <!--begin::Total Profit-->
                      <a href="manage_category.php" style="text-decoration: none;">
                      <div class="m-widget24">
                        <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            Category

                          </h4>
                          <br>
                          <span class="m-widget24__desc">
                            All Restaurant Category
                          </span>
                          <span class="m-widget24__stats m--font-brand">
                            <?php echo $total_category;?>
                          </span>
                          <div class="m--space-40"></div>
                            
                        </div>
                      </div>
                      </a>
                      <!--end::Total Profit-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                      <!--begin::New Feedbacks-->
                      <a href="manage_restaurants.php" style="text-decoration: none;">
                      <div class="m-widget24">
                        <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            Restaurants
                          </h4>
                          <br>
                          <span class="m-widget24__desc">
                            All Restaurants
                          </span>
                          <span class="m-widget24__stats m--font-info">
                            <?php echo $total_restaurant;?>
                          </span>
                          <div class="m--space-40"></div>
                           
                        </div>
                      </div>
                      </a>
                      <!--end::New Feedbacks-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                      <!--begin::New Orders-->
                      <a href="manage_order_list.php" style="text-decoration: none;">
                      <div class="m-widget24">
                        <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            Orders
                          </h4>
                          <br>
                          <span class="m-widget24__desc">
                            Orders List
                          </span>
                          <span class="m-widget24__stats m--font-danger">
                            <?php echo $total_order;?>
                          </span>
                          <div class="m--space-40"></div>
                           
                        </div>
                      </div>
                    </a>
                      <!--end::New Orders-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                      <!--begin::New Users-->
                      <a href="manage_users.php" style="text-decoration: none;">
                      <div class="m-widget24">
                        <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            Users
                          </h4>
                          <br>
                          <span class="m-widget24__desc">
                            Joined Users
                          </span>
                          <span class="m-widget24__stats m--font-success">
                            <?php echo $total_users;?>
                          </span>
                          <div class="m--space-40"></div>
                           
                        </div>
                      </div>
                    </a>
                      <!--end::New Users-->
                    </div>

                  </div>
                </div>
              </div>            
              <!--end:: Widgets/Stats-->
 
           
            
          </div>
        </div>
      </div>
      <!-- end:: Body -->



        
<?php include("includes/footer.php");?>       
