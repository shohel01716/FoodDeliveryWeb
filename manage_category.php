<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");

	if(isset($_POST['cat_search']))
   {
      
     $quotes_qry="SELECT * FROM tbl_category 
      WHERE tbl_category.category_name like '%".addslashes($_POST['search_value'])."%' ORDER BY tbl_category.category_name DESC"; 
     $result=mysqli_query($mysqli,$quotes_qry);
    
   }
   else
   {
	 

      $tableName="tbl_category";   
      $targetpage = "manage_category.php"; 
      $limit = 10; 
      
      $query = "SELECT COUNT(*) as num FROM $tableName";
      $total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
      $total_pages = $total_pages['num'];
      
      $stages = 1;
      $page=0;
      if(isset($_GET['page'])){
      $page = mysqli_real_escape_string($mysqli,$_GET['page']);
      }
      if($page){
        $start = ($page - 1) * $limit; 
      }else{
        $start = 0; 
        } 
      
     $quotes_qry="SELECT * FROM tbl_category 
      ORDER BY tbl_category.cid DESC LIMIT $start, $limit"; 
     $result=mysqli_query($mysqli,$quotes_qry);


   }
	
	if(isset($_GET['cat_id']))
	{
 
		$cat_res=mysqli_query($mysqli,'SELECT * FROM tbl_category WHERE cid=\''.$_GET['cat_id'].'\'');
		$cat_res_row=mysqli_fetch_assoc($cat_res);


		if($cat_res_row['category_image']!="")
	    {
	    	unlink('images/'.$cat_res_row['category_image']);
		}
 
		Delete('tbl_category','cid='.$_GET['cat_id'].'');

      
		$_SESSION['msg']="12";
		header( "Location:manage_category.php");
		exit;
		
	}	
	 
?>
                
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
           
          <div class="m-content">
            
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Manage Categories
                       
                    </h3>
                  </div>
                </div>
                <div class="m-portlet__head-tools">
                   
                </div>
              </div>
              <div class="m-portlet__body">

                <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?> 

                <!--begin: Search Form -->

                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                  <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                      <div class="form-group m-form__group row align-items-center">
                        <form  method="post" action="" class="m-form">

                            <div class="col-md-12">
                              <div class="m-input-icon m-input-icon--left">
                                <div class="input-group">
                                  <input type="text" name="search_value" class="form-control form-control-warning" placeholder="Search for..." required>
                                  <span class="input-group-btn">
                                    <button class="btn btn-brand" type="submit" name="cat_search">
                                      Go!
                                    </button>
                                  </span>
                                </div>
                              </div>
                            </div>
                        </form>    
                      </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                      <a href="add_category.php?add=yes" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                        <span>
                          <i class="la la-plus"></i>
                          <span>
                            Add Category
                          </span>
                        </span>
                      </a>
                      <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                  </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="m_datatable" id="local_data">
                    <table class="table">
              <thead class="thead-default">
                <tr>                  
                   <th>Category</th>
                  <th>Category Image</th>
                  <th class="cat_action_list">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
            $i=0;
            while($row=mysqli_fetch_array($result))
            {         
        ?>
                <tr scope="row">                 
                   <td><?php echo $row['category_name'];?></td>
                  <td><img src="images/<?php echo $row['category_image'];?>" width="150" height="100" /></td>
                  <td>
                    <a href="add_category.php?cat_id=<?php echo $row['cid'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">              <i class="la la-edit"></i>            </a>

                  <a href="?cat_id=<?php echo $row['cid'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick="return confirm('Are you sure you want to delete this category?');">              <i class="la la-trash"></i>           </a>
                     
                </tr>
                <?php
            
            $i++;
              }
        ?> 
              </tbody>
            </table>

                </div>
          <div class="col-md-12 col-xs-12">
                <div class="pagination_item_block">
                  <nav>
                    <?php if(!isset($_POST["cat_search"])){ include("pagination.php");}?>
                  </nav>
                </div>
          </div>


                <!--end: Datatable -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->
        
<?php include("includes/footer.php");?>       
