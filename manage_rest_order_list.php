<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");
    

    if(isset($_GET['restaurant_id']))
    {
         
      $rest_qry="SELECT * FROM tbl_restaurants where id='".$_GET['restaurant_id']."'";
      $rest_result=mysqli_query($mysqli,$rest_qry);
      $rest_row=mysqli_fetch_assoc($rest_result);

    }
    else
    {
      header("Location:manage_restaurants.php");
      exit; 
    }
      

      $tableName="tbl_order_items";   
      $targetpage = "manage_rest_order_list.php"; 
      $limit = 10; 
      
      $query = "SELECT COUNT(*) as num FROM $tableName  
      WHERE tbl_order_items.rest_id='".$_GET['restaurant_id']."'
      ORDER BY tbl_order_items.id DESC";
      $total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
      $total_pages = $total_pages['num'];
      
      $stages = 1;
      $page=0;
      if(isset($_GET['page'])){
      $page = mysqli_real_escape_string($mysqli,$_GET['page']);
      }
      if($page){
        $start = ($page - 1) * $limit; 
      }else{
        $start = 0; 
        }


     $data_qry="SELECT * FROM tbl_order_items
       WHERE tbl_order_items.rest_id='".$_GET['restaurant_id']."' 
       GROUP BY(order_id) ORDER BY tbl_order_items.id DESC LIMIT $start, $limit"; 
     $result=mysqli_query($mysqli,$data_qry);
 
	
	if(isset($_GET['order_id']))
	{

		Delete('tbl_order_items','order_id="'.$_GET['order_id'].'"');
    Delete('tbl_order_details','order_unique_id="'.$_GET['order_id'].'"');
 
		$_SESSION['msg']="12";
		header( "Location:manage_rest_order_list.php?restaurant_id=".$_GET['restaurant_id']);
		exit;
		
	}	

   //order status
if(isset($_GET['status_pending_id']))
{
   $data = array('status'  =>  $_GET['status_value']);
  
   $edit_status=Update('tbl_order_details', $data, "WHERE order_unique_id = '".$_GET['status_pending_id']."'");
  
   //$_SESSION['msg']="14";
   header( "Location:manage_rest_order_list.php?restaurant_id=".$_GET['restaurant_id']);
   exit;
   exit;
}

  function get_user_info($user_id)
   {
    global $mysqli;

    $query1="SELECT * FROM tbl_users
    WHERE tbl_users.id='".$user_id."'";

  $sql1 = mysqli_query($mysqli,$query1)or die(mysqli_error());
  $data1 = mysqli_fetch_assoc($sql1);

  return $data1;
   }

   function get_order_status($order_id)
   {
      global $mysqli;

      $query1="SELECT * FROM tbl_order_details
      WHERE tbl_order_details.order_unique_id='".$order_id."'";

    $sql1 = mysqli_query($mysqli,$query1)or die(mysqli_error());
    $data1 = mysqli_fetch_assoc($sql1);

    return $data1['status'];
   }
	 
?>
                
     <div class="m-grid__item m-grid__item--fluid m-wrapper">
          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title ">
                  Restaurant : <?php echo $rest_row['restaurant_name'];?>
                </h3>
              </div>
              <div>
                 
              </div>
            </div>
          </div>
          <!-- END: Subheader -->
          <div class="m-content">
            <div class="row">
              <div class="col-lg-3">
                <div class="m-portlet m-portlet--full-height ">
                  <div class="m-portlet__body">
                    <div class="m-card-profile">
                      <div class="m-card-profile__title m--hide">
                        Your Dashboard
                      </div>
                      <div class="m-card-profile__pic">
                        <div class="m-card-profile__pic-wrapper">
                          <img src="images/<?php echo $rest_row['restaurant_image'];?>" alt=""/>
                        </div>
                      </div>
                      <div class="m-card-profile__details">
                        <span class="m-card-profile__name">
                          <?php echo $rest_row['restaurant_name'];?>
                        </span>
                         
                          <?php echo $rest_row['restaurant_address'];?>
                         
                      </div>
                    </div>
                    <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                      <li class="m-nav__separator m-nav__separator--fit"></li>
                      <li class="m-nav__section m--hide">
                        <span class="m-nav__section-text">
                          Section
                        </span>
                      </li>
                       <li class="m-nav__item">
                        <a href="restaurant_view.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon fa fa-dashboard "></i>
                          <span class="m-nav__link-text">
                            Dashboard
                          </span>
                        </a>
                      </li>
                      <li class="m-nav__item">
                        <a href="manage_menu_category.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon flaticon-share"></i>
                          <span class="m-nav__link-text">
                            Menu Category
                          </span>
                        </a>
                      </li>
                      <li class="m-nav__item">
                        <a href="manage_menu_list.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon flaticon-chat-1"></i>
                          <span class="m-nav__link-text">
                            Menu List
                          </span>
                        </a>
                      </li>
                       <li class="m-nav__item">
                        <a href="manage_rest_order_list.php?restaurant_id=<?php echo $rest_row['id'];?>" class="m-nav__link">
                          <i class="m-nav__link-icon fa fa-cart-arrow-down"></i>
                          <span class="m-nav__link-text">
                            Order List
                          </span>
                        </a>
                      </li>
                    </ul>
                    <div class="m-portlet__body-separator"></div>
                    
                  </div>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="m-content">
            
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Order List
                       
                    </h3>
                  </div>
                </div>
                <div class="m-portlet__head-tools">
                   
                </div>
              </div>
              <div class="m-portlet__body">

                <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?> 

                 
                <!--begin: Datatable -->
                <div class="m_datatable" id="local_data">
                    <table class="table">
              <thead class="thead-default">
                <tr>                  
                   <th>Order ID</th>
                   <th>User Name</th>
                   <th>User Phone</th>
                   <th>Status</th>
                   <th class="cat_action_list">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
            $i=0;
            while($row=mysqli_fetch_array($result))
            {         
        ?>
                <tr scope="row">                 
                   <td><a href="manage_rest_order_list_view.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>&user_id=<?php echo $row['user_id']?>&order_id=<?php echo $row['order_id'];?>" title="View Order"><?php echo $row['order_id'];?></a></td>
                   <td><?php echo get_user_info($row['user_id'])['name'];?></td>
                   <td><?php echo get_user_info($row['user_id'])['phone'];?></td>
                   <td>
                      <div class="btn-group">
                        <button type="button" class="btn <?php if(get_order_status($row['order_id'])=="Complete"){?>btn-success<?php }else if(get_order_status($row['order_id'])=="Process"){?> btn-warning <?php }else{?>btn-danger<?php }?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo get_order_status($row['order_id']);?></button>
                        <div class="dropdown-menu" x-placement="top-start">
                            <a class="dropdown-item" href="manage_rest_order_list.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>&status_pending_id=<?php echo $row['order_id'];?>&status_value=Pending">Pending</a>
                            <a class="dropdown-item" href="manage_rest_order_list.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>&status_pending_id=<?php echo $row['order_id'];?>&status_value=Process">Process</a>
                            <a class="dropdown-item" href="manage_rest_order_list.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>&status_pending_id=<?php echo $row['order_id'];?>&status_value=Complete">Complete</a>                            
                             
                        </div>
                      </div>
                    </td>
                   <td>
                          
                   <a href="manage_rest_order_list_view.php?restaurant_id=<?php echo $_GET['restaurant_id'];?>&user_id=<?php echo $row['user_id']?>&order_id=<?php echo $row['order_id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="View Order">              <i class="la la-eye"></i>            </a>       

                  <a href="?order_id=<?php echo $row['order_id'];?>&restaurant_id=<?php echo $_GET['restaurant_id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick="return confirm('Are you sure you want to delete this order?');">              <i class="la la-trash"></i>           </a>
                     
                </tr>
                <?php
            
            $i++;
              }
        ?>    
              
              </tbody>
            </table>

                </div>
                <div class="col-md-12 col-xs-12">
                <div class="pagination_item_block">
                  <nav>
                    <?php include("pagination_rest.php");?>
                  </nav>
                </div>
          </div>
                <!--end: Datatable -->
              </div>
            </div>
          </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->
        
<?php include("includes/footer.php");?>       
