<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");

	if(isset($_POST['cat_search']))
   {
      
     $data_qry="SELECT * FROM tbl_restaurants
      LEFT JOIN tbl_category ON tbl_category.cid= tbl_restaurants.cat_id 
      WHERE tbl_restaurants.restaurant_name like '%".addslashes($_POST['search_value'])."%' ORDER BY tbl_restaurants.restaurant_name DESC"; 
     $result=mysqli_query($mysqli,$data_qry);
    
   }
   else
   {
	 

      $tableName="tbl_restaurants";   
      $targetpage = "manage_restaurants.php"; 
      $limit = 10; 
      
      $query = "SELECT COUNT(*) as num FROM $tableName LEFT JOIN tbl_category ON tbl_category.cid= tbl_restaurants.cat_id ORDER BY tbl_restaurants.restaurant_name DESC";
      $total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
      $total_pages = $total_pages['num'];
      
      $stages = 1;
      $page=0;
      if(isset($_GET['page'])){
      $page = mysqli_real_escape_string($mysqli,$_GET['page']);
      }
      if($page){
        $start = ($page - 1) * $limit; 
      }else{
        $start = 0; 
        } 
      
     $quotes_qry="SELECT * FROM tbl_restaurants
      LEFT JOIN tbl_category ON tbl_category.cid= tbl_restaurants.cat_id 
      ORDER BY tbl_restaurants.id DESC LIMIT $start, $limit"; 
     $result=mysqli_query($mysqli,$quotes_qry);


   }
	
	if(isset($_GET['restaurant_id']))
	{
 
		$cat_res=mysqli_query($mysqli,'SELECT * FROM tbl_restaurants WHERE id=\''.$_GET['restaurant_id'].'\'');
		$cat_res_row=mysqli_fetch_assoc($cat_res);


		if($cat_res_row['restaurant_image']!="")
	    {
	    	unlink('images/'.$cat_res_row['restaurant_image']);
		}
 
		Delete('tbl_restaurants','id='.$_GET['restaurant_id'].'');

      
		$_SESSION['msg']="12";
		header( "Location:manage_restaurants.php");
		exit;
		
	}	


  //Active and Deactive featured video
if(isset($_GET['featured_deactive_id']))
{
   $data = array('featured_restaurant'  =>  '0');
  
   $edit_status=Update('tbl_restaurants', $data, "WHERE id = '".$_GET['featured_deactive_id']."'");
  
   $_SESSION['msg']="14";
   header( "Location:manage_restaurants.php");
   exit;
}
if(isset($_GET['featured_active_id']))
{
    $data = array('featured_restaurant'  =>  '1');
    
    $edit_status=Update('tbl_restaurants', $data, "WHERE id = '".$_GET['featured_active_id']."'");
    
    $_SESSION['msg']="13";   
    header( "Location:manage_restaurants.php");
    exit;
}  
   
  //Active and Deactive status video
if(isset($_GET['status_deactive_id']))
{
   $data = array('status'  =>  '0');
  
   $edit_status=Update('tbl_restaurants', $data, "WHERE id = '".$_GET['status_deactive_id']."'");
  
   $_SESSION['msg']="14";
   header( "Location:manage_restaurants.php");
   exit;
}
if(isset($_GET['status_active_id']))
{
    $data = array('status'  =>  '1');
    
    $edit_status=Update('tbl_restaurants', $data, "WHERE id = '".$_GET['status_active_id']."'");
    
    $_SESSION['msg']="13";   
    header( "Location:manage_restaurants.php");
    exit;
}
	 
?>
                
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
           
          <div class="m-content">
            
            <div class="m-portlet m-portlet--mobile">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Manage Restaurants
                       
                    </h3>
                  </div>
                </div>
                <div class="m-portlet__head-tools">
                   
                </div>
              </div>
              <div class="m-portlet__body">

                <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?> 

                <!--begin: Search Form -->

                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                  <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                      <div class="form-group m-form__group row align-items-center">
                        <form  method="post" action="" class="m-form">

                            <div class="col-md-12">
                              <div class="m-input-icon m-input-icon--left">
                                <div class="input-group">
                                  <input type="text" name="search_value" class="form-control form-control-warning" placeholder="Search for..." required>
                                  <span class="input-group-btn">
                                    <button class="btn btn-brand" type="submit" name="cat_search">
                                      Go!
                                    </button>
                                  </span>
                                </div>
                              </div>
                            </div>
                        </form>    
                      </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                      <a href="add_restaurant.php?add=yes" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                        <span>
                          <i class="la la-plus"></i>
                          <span>
                            Add Restaurant
                          </span>
                        </span>
                      </a>
                      <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                  </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="m_datatable" id="local_data">
                    <table class="table">
              <thead class="thead-default">
                <tr>                  
                   <th>Category</th>
                   <th>Restaurant Name</th>
                   <th>Restaurant Image</th>
                   <th>Featured</th>
                   <th>Status</th>
                   <th>View</th>
                  <th class="cat_action_list">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
            $i=0;
            while($row=mysqli_fetch_array($result))
            {         
        ?>
                <tr scope="row">                 
                   <td><?php echo $row['category_name'];?></td>
                   <td><a href="add_restaurant.php?restaurant_id=<?php echo $row['id'];?>" style="text-decoration: none;"><?php echo $row['restaurant_name'];?></a></td>
                  <td><img src="images/<?php echo $row['restaurant_image'];?>" width="150" height="100" /></td>
                                     <td>
                  <?php if($row['featured_restaurant']!="0"){?>
                  <a href="manage_restaurants.php?featured_deactive_id=<?php echo $row['id'];?>" title="Change Status"><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span style="font-size: 12px;
    font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Enable</span></span></a>

                  <?php }else{?>
                  <a href="manage_restaurants.php?featured_active_id=<?php echo $row['id'];?>" title="Change Status"><span class="badge badge-danger badge-icon"><i class="fa fa-close" aria-hidden="true"></i><span style="font-size: 12px;
    font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Disable </span></span></a>
                  <?php }?>
                  </td>

                  <td>
                  <?php if($row['status']!="0"){?>
                  <a href="manage_restaurants.php?status_deactive_id=<?php echo $row['id'];?>" title="Change Status"><span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span style="font-size: 12px;
    font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Enable</span></span></a>

                  <?php }else{?>
                  <a href="manage_restaurants.php?status_active_id=<?php echo $row['id'];?>" title="Change Status"><span class="badge badge-danger badge-icon"><i class="fa fa-close" aria-hidden="true"></i><span style="font-size: 12px;
    font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> Disable </span></span></a>
                  <?php }?>
                  </td>
                  <td>
                    <a href="restaurant_view.php?restaurant_id=<?php echo $row['id'];?>" title="View Restaurant"><span class="badge badge-success badge-icon"><i class="fa fa-eye" aria-hidden="true"></i><span style="font-size: 12px;
    font-weight: 500;line-height: 16px;display: inline-block;margin-left: 3px;"> View Menu</span></span></a>
                  </td>  
                  <td> 

                    <a href="add_restaurant.php?restaurant_id=<?php echo $row['id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">              <i class="la la-edit"></i>            </a>

                  <a href="?restaurant_id=<?php echo $row['id'];?>" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete" onclick="return confirm('Are you sure you want to delete this restaurant?');">              <i class="la la-trash"></i>           </a>
                     
                </tr>
                <?php
            
            $i++;
              }
        ?> 
              </tbody>
            </table>

                </div>
          <div class="col-md-12 col-xs-12">
                <div class="pagination_item_block">
                  <nav>
                    <?php if(!isset($_POST["cat_search"])){ include("pagination.php");}?>
                  </nav>
                </div>
          </div>


                <!--end: Datatable -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->
        
<?php include("includes/footer.php");?>       
