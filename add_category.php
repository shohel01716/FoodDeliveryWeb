<?php include("includes/header.php");

require("includes/function.php");
require("language/language.php");
require_once("thumbnail_images.class.php");

   
  if(isset($_POST['submit']) and isset($_GET['add']))
  {
  
     $category_image=rand(0,99999)."_".$_FILES['category_image']['name'];
       
       //Main Image
     $tpath1='images/'.$category_image;        
       $pic1=compress_image($_FILES["category_image"]["tmp_name"], $tpath1, 80);
    
       $data = array( 
         'category_name'  =>  $_POST['category_name'],
         'category_image'  =>  $category_image
          );    

    $qry = Insert('tbl_category',$data);  
 

    $_SESSION['msg']="10";
 
    header( "Location:manage_category.php");
    exit; 

     
    
  }
  
  if(isset($_GET['cat_id']))
  {
       
      $qry="SELECT * FROM tbl_category where cid='".$_GET['cat_id']."'";
      $result=mysqli_query($mysqli,$qry);
      $row=mysqli_fetch_assoc($result);

  }
  if(isset($_POST['submit']) and isset($_POST['cat_id']))
  {
     
     if($_FILES['category_image']['name']!="")
     {    


        $img_res=mysqli_query($mysqli,'SELECT * FROM tbl_category WHERE cid='.$_GET['cat_id'].'');
          $img_res_row=mysqli_fetch_assoc($img_res);
      

          if($img_res_row['category_image']!="")
            {
                unlink('images/'.$img_res_row['category_image']);
           }

           $category_image=rand(0,99999)."_".$_FILES['category_image']['name'];
       
             //Main Image
           $tpath1='images/'.$category_image;        
             $pic1=compress_image($_FILES["category_image"]["tmp_name"], $tpath1, 80);
         
          
                    $data = array(
                       'category_name'  =>  $_POST['category_name'],
                      'category_image'  =>  $category_image
                    );

          $category_edit=Update('tbl_category', $data, "WHERE cid = '".$_POST['cat_id']."'");

     }
     else
     {

           $data = array(
                 'category_name'  =>  $_POST['category_name']
            );  
 
               $category_edit=Update('tbl_category', $data, "WHERE cid = '".$_POST['cat_id']."'");

     }

     
    $_SESSION['msg']="11"; 
    header( "Location:add_category.php?cat_id=".$_POST['cat_id']);
    exit;
 
  }
 

?>       


        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
           
          <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      <?php if(isset($_GET['cat_id'])){?>Edit<?php }else{?>Add<?php }?> Category
                    </h3>
                  </div>
                </div>
              </div>
              <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?>
              <!--begin::Form-->
              <form action="" name="addeditcategory" method="post" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
              
                 <input  type="hidden" name="cat_id" value="<?php echo $_GET['cat_id'];?>" />

                <div class="m-portlet__body">
             
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Category Name
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="category_name" id="category_name" value="<?php if(isset($_GET['cat_id'])){echo $row['category_name'];}?>" placeholder="Enter Category Name">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Category Image
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                                <input type="file" id="category_image" name="category_image" class="form-control m-input">
                                  <br/>
                                  <?php if(isset($_GET['cat_id']) and $row['category_image']!="") {?>
                                    <img type="image" src="images/<?php echo $row['category_image'];?>" width="150" height="100" alt="image" />                                   
                                  <?php }?>

                     </div>
                  </div>
                   
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions">
                    <div class="row">
                      <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" name="submit" class="btn btn-brand">
                          Save
                        </button>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>
        </div>
      </div>
      <!-- end:: Body -->

        
<?php include("includes/footer.php");?>       
