<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");
	 
	
	if(isset($_SESSION['id']))
	{
			 
		$qry="select * from tbl_admin where id='".$_SESSION['id']."'";
		 
		$result=mysqli_query($mysqli,$qry);
		$row=mysqli_fetch_assoc($result);

	}
	if(isset($_POST['submit']))
	{
		if($_FILES['image']['name']!="")
		 {		
 

				$img_res=mysqli_query($mysqli,'SELECT * FROM tbl_admin WHERE id='.$_SESSION['id'].'');
			  $img_res_row=mysqli_fetch_assoc($img_res);
			

			    if($img_res_row['image']!="")
		        {
					 
					     unlink('images/'.$img_res_row['image']);
			     }

 				   $image="profile.png";
				   $pic1=$_FILES['image']['tmp_name'];
				   $tpath1='images/'.$image;
					
					copy($pic1,$tpath1);
 
					$data = array( 
                  'full_name'  =>  $_POST['full_name'],
                  'email'  =>  $_POST['email'],
                  'phone'  =>  $_POST['phone'],
 							    'username'  =>  $_POST['username'],
							    'password'  =>  $_POST['password'],							    
							    'image'  =>  $image
							    );
					
					$channel_edit=Update('tbl_admin', $data, "WHERE id = '".$_SESSION['id']."'"); 

		 }
		 else
		 {
       
					$data = array(
                  'full_name'  =>  $_POST['full_name'],
                  'email'  =>  $_POST['email'],
                  'phone'  =>  $_POST['phone'], 
							    'username'  =>  $_POST['username'],
							    'password'  =>  $_POST['password']
							    );
					
					$channel_edit=Update('tbl_admin', $data, "WHERE id = '".$_SESSION['id']."'"); 
		}

		$_SESSION['msg']="11"; 
		header( "Location:profile.php");
		exit;
		 
	}


?>
 
	 <div class="m-grid__item m-grid__item--fluid m-wrapper">
          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title ">
                  My Profile
                </h3>
              </div>
              <div>
                 
              </div>
            </div>
          </div>
          <!-- END: Subheader -->
          <div class="m-content">
            <div class="row">
              <div class="col-lg-3">
                <div class="m-portlet m-portlet--full-height ">
                  <div class="m-portlet__body">
                    <div class="m-card-profile">
                      <div class="m-card-profile__title m--hide">
                        Your Profile
                      </div>
                      <div class="m-card-profile__pic">
                        <div class="m-card-profile__pic-wrapper">
                          <img src="images/<?php echo $row['image'];?>" alt=""/>
                        </div>
                      </div>
                      <div class="m-card-profile__details">
                        <span class="m-card-profile__name">
                          <?php echo $row['full_name'];?>
                        </span>
                        <a href="mailto:<?php echo PROFILE_EMAIL;?>" class="m-card-profile__email m-link">
                          <?php echo $row['email'];?>
                        </a>
                      </div>
                    </div>
                    
                    <div class="m-portlet__body-separator"></div>
                    
                  </div>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs ">
                  <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                      <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                        <li class="nav-item m-tabs__item">
                          <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                            <i class="flaticon-share m--hide"></i>
                            Update Profile
                          </a>
                        </li>
                        
                      </ul>
                    </div>
                     
                  </div>
                  <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1">
                      <form class="m-form m-form--fit m-form--label-align-right" action="profile.php" method="post" enctype="multipart/form-data">
                        <div class="m-portlet__body">
                           
                          <div class="form-group m-form__group row">
                            <div class="col-10 ml-auto">
                              <h3 class="m-form__section">
                                Personal Details
                              </h3>
                            </div>
                          </div>
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                              Full Name
                            </label>
                            <div class="col-7">
                              <input class="form-control m-input" type="text" name="full_name" value="<?php echo $row['full_name'];?>">
                            </div>
                          </div>
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                              Email
                            </label>
                            <div class="col-7">
                              <input class="form-control m-input" type="text" name="email" value="<?php echo $row['email'];?>">
                            </div>
                          </div>
                           
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                              Phone No.
                            </label>
                            <div class="col-7">
                              <input class="form-control m-input" type="text" name="phone" value="<?php echo $row['phone'];?>">
                            </div>
                          </div>
                           <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                              Profile Image
                            </label>
                            <div class="col-7">
                               <input type="file" id="" name="image" class="form-control m-input">
                             </div>
                          </div>
                           
                          <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                          <div class="form-group m-form__group row">
                            <div class="col-10 ml-auto">
                              <h3 class="m-form__section">
                                Login Details
                              </h3>
                            </div>
                          </div>
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                              User Name
                            </label>
                            <div class="col-7">
                              <input class="form-control m-input" type="text" name="username" value="<?php echo $row['username'];?>">
                            </div>
                          </div>
                          <div class="form-group m-form__group row">
                            <label for="example-text-input" class="col-2 col-form-label">
                              password
                            </label>
                            <div class="col-7">
                              <input class="form-control m-input" type="password" name="password" value="" required>
                            </div>
                          </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                          <div class="m-form__actions">
                            <div class="row">
                              <div class="col-2"></div>
                              <div class="col-7">
                                <button type="submit" name="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                  Save changes
                                </button>
                               
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane active" id="m_user_profile_tab_2"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end:: Body -->

        
<?php include("includes/footer.php");?>       
