<?php include("includes/header.php");

require("includes/function.php");
require("language/language.php");
require_once("thumbnail_images.class.php");
  
  //Get program
  $cat_qry="SELECT * FROM tbl_category ORDER BY category_name";
  $cat_result=mysqli_query($mysqli,$cat_qry); 
   
  if(isset($_POST['submit']) and isset($_GET['add']))
  {
  
     $restaurant_image=rand(0,99999)."_".$_FILES['restaurant_image']['name'];
       
       //Main Image
     $tpath1='images/'.$restaurant_image;        
       $pic1=compress_image($_FILES["restaurant_image"]["tmp_name"], $tpath1, 80);
    
       $data = array( 
         'cat_id'  =>  $_POST['cat_id'],
         'restaurant_name'  =>  $_POST['restaurant_name'],
         'restaurant_type'  =>  $_POST['restaurant_type'],
         'restaurant_address'  =>  $_POST['restaurant_address'],
         'restaurant_image'  =>  $restaurant_image,
         'restaurant_open_mon'  =>  $_POST['restaurant_open_mon'],
         'restaurant_open_tues'  =>  $_POST['restaurant_open_tues'],
         'restaurant_open_wed'  =>  $_POST['restaurant_open_wed'],
         'restaurant_open_thur'  =>  $_POST['restaurant_open_thur'],
         'restaurant_open_fri'  =>  $_POST['restaurant_open_fri'],
         'restaurant_open_sat'  =>  $_POST['restaurant_open_sat'],
         'restaurant_open_sun'  =>  $_POST['restaurant_open_sun']
          );    

    $qry = Insert('tbl_restaurants',$data);  
 

    $_SESSION['msg']="10";
 
    header( "Location:manage_restaurants.php");
    exit; 

     
    
  }
  
  if(isset($_GET['restaurant_id']))
  {
       
      $qry="SELECT * FROM tbl_restaurants where id='".$_GET['restaurant_id']."'";
      $result=mysqli_query($mysqli,$qry);
      $row=mysqli_fetch_assoc($result);

  }
  if(isset($_POST['submit']) and isset($_POST['restaurant_id']))
  {
     
     if($_FILES['restaurant_image']['name']!="")
     {    


        $img_res=mysqli_query($mysqli,'SELECT * FROM tbl_restaurants WHERE id='.$_POST['restaurant_id'].'');
          $img_res_row=mysqli_fetch_assoc($img_res);
      

          if($img_res_row['restaurant_image']!="")
            {
                unlink('images/'.$img_res_row['restaurant_image']);
           }

           $restaurant_image=rand(0,99999)."_".$_FILES['restaurant_image']['name'];
       
             //Main Image
           $tpath1='images/'.$restaurant_image;        
             $pic1=compress_image($_FILES["restaurant_image"]["tmp_name"], $tpath1, 80);
         
          
                    $data = array(
                       'cat_id'  =>  $_POST['cat_id'],
                       'restaurant_name'  =>  $_POST['restaurant_name'],
                       'restaurant_type'  =>  $_POST['restaurant_type'],
                       'restaurant_address'  =>  $_POST['restaurant_address'],
                       'restaurant_image'  =>  $restaurant_image,
                       'restaurant_open_mon'  =>  $_POST['restaurant_open_mon'],
                       'restaurant_open_tues'  =>  $_POST['restaurant_open_tues'],
                       'restaurant_open_wed'  =>  $_POST['restaurant_open_wed'],
                       'restaurant_open_thur'  =>  $_POST['restaurant_open_thur'],
                       'restaurant_open_fri'  =>  $_POST['restaurant_open_fri'],
                       'restaurant_open_sat'  =>  $_POST['restaurant_open_sat'],
                       'restaurant_open_sun'  =>  $_POST['restaurant_open_sun']
                    );

          $restaurant_edit=Update('tbl_restaurants', $data, "WHERE id = '".$_POST['restaurant_id']."'");

     }
     else
     {

           $data = array(
                 'cat_id'  =>  $_POST['cat_id'],  
                 'restaurant_name'  =>  $_POST['restaurant_name'],
                 'restaurant_type'  =>  $_POST['restaurant_type'],
                 'restaurant_address'  =>  $_POST['restaurant_address'],
                 'restaurant_open_mon'  =>  $_POST['restaurant_open_mon'],
                 'restaurant_open_tues'  =>  $_POST['restaurant_open_tues'],
                 'restaurant_open_wed'  =>  $_POST['restaurant_open_wed'],
                 'restaurant_open_thur'  =>  $_POST['restaurant_open_thur'],
                 'restaurant_open_fri'  =>  $_POST['restaurant_open_fri'],
                 'restaurant_open_sat'  =>  $_POST['restaurant_open_sat'],
                 'restaurant_open_sun'  =>  $_POST['restaurant_open_sun']
            );  
 
               $restaurant_edit=Update('tbl_restaurants', $data, "WHERE id = '".$_POST['restaurant_id']."'");

     }

     
    $_SESSION['msg']="11"; 
    header( "Location:add_restaurant.php?restaurant_id=".$_POST['restaurant_id']);
    exit;
 
  }
 

?>       
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

        
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
           
          <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
              <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                  <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      <?php if(isset($_GET['restaurant_id'])){?>Edit<?php }else{?>Add<?php }?> Restaurant
                    </h3>
                  </div>
                </div>
              </div>
              <?php if(isset($_SESSION['msg'])){?> 
              <div class="m-portlet__body form-group m-form__group m--margin-top-10" style="padding-bottom: 5px; padding-top: 5px;">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                          <?php echo $client_lang[$_SESSION['msg']] ; ?>
                </div>
              </div>
              <?php unset($_SESSION['msg']);}?>
              <!--begin::Form-->
              <form action="" name="addedit_from" method="post" class="m-form m-form--fit m-form--label-align-right" enctype="multipart/form-data">
              
                 <input  type="hidden" name="restaurant_id" value="<?php echo $_GET['restaurant_id'];?>" />

                <div class="m-portlet__body">
                  
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Category
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <select name="cat_id" id="m_select1" class="form-control m-select1" required>
                        <option value="">--Select Category--</option>
                        <?php
                            while($cat_row=mysqli_fetch_array($cat_result))
                            {
                        ?>                       
                        <option value="<?php echo $cat_row['cid'];?>" <?php if(isset($_GET['restaurant_id']) && $cat_row['cid']==$row['cat_id']) {?>selected<?php }?>><?php echo $cat_row['category_name'];?></option>                           
                        <?php
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Restaurant Name
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="restaurant_name" id="restaurant_name" value="<?php if(isset($_GET['restaurant_id'])){echo $row['restaurant_name'];}?>" placeholder="Enter Restaurant Name">
                    </div>
                  </div>
				  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Restaurant Type
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <select name="restaurant_type" id="restaurant_type" class="form-control m-input">
                                <option value="Veg" <?php if($row['restaurant_type']=='Veg'){?>selected<?php }?>>Veg</option>
                                <option value="Non Veg" <?php if($row['restaurant_type']=='Non Veg'){?>selected<?php }?>>Non Veg</option>
                                <option value="Veg/Non Veg" <?php if($row['restaurant_type']=='Veg/Non Veg'){?>selected<?php }?>>Veg/Non Veg</option>
                            </select> 
                                
                    </div>
                  </div>
				  
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Restaurant Address
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      
                      <textarea name="restaurant_address" id="restaurant_address" class="form-control m-input"><?php if(isset($_GET['restaurant_id'])){echo $row['restaurant_address'];}?></textarea>
 
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Restaurant Image
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                                <input type="file" id="restaurant_image" name="restaurant_image" class="form-control m-input">
                                  <br/>
                                  <?php if(isset($_GET['restaurant_id']) and $row['restaurant_image']!="") {?>
                                    <img type="image" src="images/<?php echo $row['restaurant_image'];?>" width="150" height="100" alt="image" />                                   
                                  <?php }?>

                     </div>
                  </div>
                  <hr>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      &nbsp;
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                        <h4>Open Hours</h4>
                    </div>
                  </div>
                  
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Monday
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="restaurant_open_mon" id="restaurant_open_mon" value="<?php if(isset($_GET['restaurant_id'])){echo $row['restaurant_open_mon'];}?>" placeholder="11AM–11PM ">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Tuesday
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="restaurant_open_tues" id="restaurant_open_tues" value="<?php if(isset($_GET['restaurant_id'])){echo $row['restaurant_open_tues'];}?>" placeholder="11AM–11PM">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Wednesday
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="restaurant_open_wed" id="restaurant_open_wed" value="<?php if(isset($_GET['restaurant_id'])){echo $row['restaurant_open_wed'];}?>" placeholder="11AM–11PM">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Thursday
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="restaurant_open_thur" id="restaurant_open_thur" value="<?php if(isset($_GET['restaurant_id'])){echo $row['restaurant_open_thur'];}?>" placeholder="11AM–11PM">
                    </div>
                  </div> 
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Friday
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="restaurant_open_fri" id="restaurant_open_fri" value="<?php if(isset($_GET['restaurant_id'])){echo $row['restaurant_open_fri'];}?>" placeholder="11AM–11PM">
                    </div>
                  </div> 
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Saturday
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="restaurant_open_sat" id="restaurant_open_sat" value="<?php if(isset($_GET['restaurant_id'])){echo $row['restaurant_open_sat'];}?>" placeholder="11AM–11PM">
                    </div>
                  </div>
                  <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                      Sunday
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-12">
                      <input type="text" class="form-control m-input" name="restaurant_open_sun" id="restaurant_open_sun" value="<?php if(isset($_GET['restaurant_id'])){echo $row['restaurant_open_sun'];}?>" placeholder="11AM–11PM">
                    </div>
                  </div> 
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions m-form__actions">
                    <div class="row">
                      <div class="col-lg-9 ml-lg-auto">
                        <button type="submit" name="submit" class="btn btn-brand">
                          Save
                        </button>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              <!--end::Form-->
            </div>
            <!--end::Portlet-->
          </div>
        </div>
      </div>
      <!-- end:: Body -->

        
<?php include("includes/footer.php");?>       
