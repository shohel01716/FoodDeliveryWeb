$(function () {
                $('#btn3').click(function () {
                    $('.myprogress3').css('width', '0');
                    $('.msg3').text('');
                    var video_local3 = $('#video_local3').val();
                    if (video_local3 == '') {
                        alert('Please enter file name and select file');
                        return;
                    }
                    var formData = new FormData();
                    formData.append('video_local3', $('#video_local3')[0].files[0]);
                    $('#btn3').attr('disabled', 'disabled');
                     $('.msg3').text('Uploading in progress...');
                    $.ajax({
                        url: 'uploadscript3.php',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        // this part is progress bar
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.myprogress3').text(percentComplete + '%');
                                    $('.myprogress3').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                        },
                        success: function (data) {
                         
                            $('#video_file_name3').val(data);
                            $('.msg3').text("File uploaded successfully!!");
                            $('#btn3').removeAttr('disabled');
                        }
                    });
                });
            });