$(function () {
                $('#btn2').click(function () {
                    $('.myprogress2').css('width', '0');
                    $('.msg2').text('');
                    var video_local2 = $('#video_local2').val();
                    if (video_local2 == '') {
                        alert('Please enter file name and select file');
                        return;
                    }
                    var formData = new FormData();
                    formData.append('video_local2', $('#video_local2')[0].files[0]);
                    $('#btn2').attr('disabled', 'disabled');
                     $('.msg2').text('Uploading in progress...');
                    $.ajax({
                        url: 'uploadscript2.php',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        // this part is progress bar
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.myprogress2').text(percentComplete + '%');
                                    $('.myprogress2').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                        },
                        success: function (data) {
                         
                            $('#video_file_name2').val(data);
                            $('.msg2').text("File uploaded successfully!!");
                            $('#btn2').removeAttr('disabled');
                        }
                    });
                });
            });